import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ServerToShow} from '../app/model/ServerToShow';
import {environment} from '../environments/environment';
import {ServerToSave} from '../app/model/ServerToSave';

@Injectable()
export class ServerService {

  constructor(private http: HttpClient) {
  }

  baseUrl = environment.apiUrl + '/servers';

  getAll(label: string, leg: boolean): Promise<Observable<ServerToShow>> {
    return this.http.get<Observable<ServerToShow>>(this.baseUrl + '/all/' + label + '/' + leg).toPromise();
  }

  getName(id: number): Promise<string> {
    // @ts-ignore
    return this.http.get<string>(this.baseUrl + '/server-name/' + id, {responseType: 'text'}).toPromise();
  }

  add(server: ServerToSave) {
    return this.http.post(this.baseUrl + '/add' + '?access_token=' + JSON.parse(window.sessionStorage.getItem('token')).access_token, server);
  }

  editServer(server: ServerToSave) {
    return this.http.post(this.baseUrl + '/edit' + '?access_token=' + JSON.parse(window.sessionStorage.getItem('token')).access_token, server);
  }

  deleteServer(id: number) {
    return this.http.post(this.baseUrl + '/delete/' + id + '?access_token=' + JSON.parse(window.sessionStorage.getItem('token')).access_token, null);
  }

  getUsersServers(): Promise<Observable<ServerToSave>> {
    return this.http.get<Observable<ServerToSave>>(this.baseUrl + '/users-servers' + '?access_token=' + JSON.parse(window.sessionStorage.getItem('token')).access_token).toPromise();
  }

  vote(id: number, nickname: string) {
    return this.http.post(environment.apiUrl + '/vote/' + id + '/' + nickname, null);
  }

  getIp() {
    return this.http.get('http://api.ipify.org/?format=json').toPromise();
  }
}

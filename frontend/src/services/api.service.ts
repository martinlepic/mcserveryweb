import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';

@Injectable()
export class ApiService {

  constructor(private http: HttpClient) {
  }

  login(loginPayload) {
    const headers = {
      'Authorization': 'Basic ' + btoa('mcservery-client:mcservery-secret'),
      'Content-type': 'application/x-www-form-urlencoded'
    };
    return this.http.post(environment.apiUrl + '/oauth/token', loginPayload, {headers});
  }
}

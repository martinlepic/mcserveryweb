import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import {User} from '../app/model/User';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {
  }

  baseUrl = environment.apiUrl + '/user';

  register(user: User) {
    return this.http.post(this.baseUrl + '/register', user);
  }
}

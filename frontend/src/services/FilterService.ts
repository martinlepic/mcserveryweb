import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';

@Injectable()
export class FilterService {

  constructor(private http: HttpClient) {
  }

  baseUrl = environment.apiUrl + '/filters';

  getAll(): Promise<Map<string, number>> {
    return this.http.get<Map<string, number>>(this.baseUrl + '/all').toPromise();
  }
}

export class ServerToSave {
  constructor(public id: number,
              public serverName: string,
              public serverIP: string,
              public serverPort: number,
              public serverWeb: string,
              public label1: string,
              public label2: string,
              public onlineMode: string,
              public votifierIP: string,
              public votifierPort: number,
              public votifierToken: string,
              public about: string,
              public added: Date) {
  }
}

export class ServerToShow {
  constructor(public id: number,
              public serverName: string,
              public serverIP: string,
              public serverPort: number,
              public serverWeb: string,
              public label1: string,
              public label2: string,
              public onlineMode: boolean,
              public version: string = null,
              public maxPlayers: number = 0,
              public onlinePlayers: number = 0,
              public online: boolean,
              public about: string,
              public added: Date,
              public position: number,
              public votes: number) {
  }
}

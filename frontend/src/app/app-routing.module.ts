import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ServersContentComponent} from './servers-content/servers-content.component';
import {RegisterComponent} from './register/register.component';
import {AdministrationComponent} from './administration/administration.component';
import {LoginComponent} from './login/login.component';
import {VoteComponent} from './vote/vote.component';
import {InfoComponent} from './info/info.component';


const routes: Routes = [
  {path: 'server-list', component: ServersContentComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'administration', component: AdministrationComponent},
  {path: 'login', component: LoginComponent},
  {path: 'faq', component: InfoComponent},
  {path: 'vote/:id', component: VoteComponent},
  {path: '**', redirectTo: '/server-list', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  alerts = [];

  constructor(private apiService: ApiService, private router: Router) {
  }

  ngOnInit() {
    window.sessionStorage.removeItem('token');
  }

  onSubmit(nick, pass) {

    const body = new HttpParams()
      .set('username', nick.value)
      .set('password', pass.value)
      .set('grant_type', 'password');

    this.apiService.login(body.toString()).subscribe(data => {
      window.sessionStorage.setItem('token', JSON.stringify(data));
      console.log(window.sessionStorage.getItem('token'));
      window.sessionStorage.setItem('user', nick.value);
      this.router.navigate(['administration']);
    }, error => {
      this.alerts = [{
        type: 'danger',
        message: 'Špatné jméno nebo heslo!'
      }];
    });
  }
  closeAlert() {
    this.alerts = [];
  }
}

window.onscroll = function () {
  scrollFunction();
}

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("top-bar").style.padding = "5px 10px";
    document.getElementById("top-bar").classList.add("my-shadow");
  } else {
    document.getElementById("top-bar").style.padding = "15px 10px";
    document.getElementById("top-bar").classList.remove("my-shadow");
  }
}

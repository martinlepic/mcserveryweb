import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ServerService} from '../../services/ServerService';
import {HttpErrorResponse} from '@angular/common/http';

declare function votifier(serverId, nick, ip): any;

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.css']
})

export class VoteComponent implements OnInit {

  serverId: string;
  serverName: string;
  alerts = [];

  constructor(private route: ActivatedRoute, private serverService: ServerService) {
    this.serverId = route.snapshot.paramMap.get('id');
    serverService.getName(+this.serverId).then(r => {
      this.serverName = r;
    });
  }

  vote(nick: HTMLInputElement) {
    if (nick.value === '') {
      this.alerts = [{
        type: 'danger',
        message: 'Musíš zadat nick!'
      }];
      return;
    }

    this.serverService.vote(+this.serverId, nick.value).subscribe(
      (response) => {
        this.serverService.getIp().then(ip => {
          votifier(this.serverId, nick.value, ip['ip']);
        });
        this.alerts = [{
          type: 'success',
          message: 'Úspěšně jsi hlasoval pro server ' + this.serverName
        }];
      },
      (error: HttpErrorResponse) => {
        this.alerts = [{
          type: 'danger',
          message: (error.error as HttpErrorResponse).message
        }];
      }
    );
  }

  closeAlert() {
    this.alerts = [];
  }

  ngOnInit() {
  }

}


import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ServerService} from '../../services/ServerService';
import {ServerToSave} from '../model/ServerToSave';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.css']
})
export class AdministrationComponent implements OnInit {

  menu = 'ADDSERVER';
  servers = [];
  serverToEdit: ServerToSave;

  alerts = [];

  constructor(private router: Router, private serverService: ServerService) {
    if (window.sessionStorage.getItem('token') == null) {
      router.navigate(['login']);
    }
  }

  closeAlert() {
    this.alerts = [];
  }

  myServers() {
    this.menu = 'MYSERVERS';
    this.serverService.getUsersServers().then(r => {
      // @ts-ignore
      this.servers = r;
    });
  }

  editServer(server: ServerToSave) {
    this.serverToEdit = server;
    this.menu = 'EDITSERVER';
  }

  deleteServer(id: number) {
    this.serverService.deleteServer(id).subscribe(
      (response) => {
        this.serverService.getUsersServers().then(r => {
          // @ts-ignore
          this.servers = r;
        });
        this.alerts = [{
          type: 'success',
          message: 'Server byl úspěšně smazán'
        }];
      },
      (error: HttpErrorResponse) => {
        this.alerts = [{
          type: 'danger',
          message: (error.error as HttpErrorResponse).message
        }];
      }
    );
  }

  logout() {
    window.sessionStorage.removeItem('token');
    this.router.navigate(['server-list']);
  }

  ngOnInit() {
  }

}

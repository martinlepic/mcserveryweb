import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ServersContentComponent} from './servers-content.component';

describe('ServersContentComponent', () => {
  let component: ServersContentComponent;
  let fixture: ComponentFixture<ServersContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ServersContentComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServersContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

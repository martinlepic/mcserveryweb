import {Component, OnInit} from '@angular/core';
import {ServerService} from '../../services/ServerService';

@Component({
  selector: 'app-servers-content',
  templateUrl: './servers-content.component.html',
  styleUrls: ['./servers-content.component.css']
})
export class ServersContentComponent implements OnInit {

  servers = []
  paginationPage = 1;
  paginationPageSize = 10;

  constructor(public serverService: ServerService) {
    serverService.getAll('all', false).then(r => {
      // @ts-ignore
      this.servers = r;
    });
  }

  scrollTop() {
    window.scroll(0, 0);
  }

  ngOnInit() {
  }

}

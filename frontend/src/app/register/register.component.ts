import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/UserService';
import {User} from '../model/User';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  alerts = [];

  constructor(private userService: UserService) {
  }

  closeAlert() {
    this.alerts = [];
  }

  register(regForm: HTMLFormElement, username: HTMLInputElement, mail: HTMLInputElement, password: HTMLInputElement) {
    if (!regForm.checkValidity()) {
      return;
    }
    const user = new User(username.value, password.value, mail.value);
    this.userService.register(user).subscribe(
      (response) => {
        this.alerts = [{
          type: 'success',
          message: 'Byl jsi úspěšně zaregistrován, nyní se můžeš přihlásit'
        }];
      },
      (error: HttpErrorResponse) => {
        this.alerts = [{
          type: 'danger',
          message: (error.error as HttpErrorResponse).message
        }];
      }
    );
    username.value = null;
    password.value = null;
    mail.value = null;
  }

  ngOnInit() {
  }

}

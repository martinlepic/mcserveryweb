import {Component, OnInit} from '@angular/core';
import {FilterService} from '../../services/FilterService';
import {ServerService} from '../../services/ServerService';
import {ServersContentComponent} from '../servers-content/servers-content.component';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})
export class FiltersComponent implements OnInit {

  counts: Map<string, number>;
  loaded: boolean;

  selectedLabel = 'Survival';
  leg = false;

  constructor(private filterService: FilterService, private serverService: ServerService, private servers: ServersContentComponent) {
    filterService.getAll().then(r => {
      this.counts = r;
      this.loaded = true;
    });
  }

  selectLabel(label: string) {
    this.selectedLabel = label;
    this.load(label, this.leg);
    if (document.getElementById('allLabel').classList.contains('active')) {
      document.getElementById('allLabel').classList.remove('active');
      document.getElementById('warezLabel').classList.add('active');
    }
  }

  selectLeg(leg: boolean) {
    this.leg = leg;
    this.load(this.selectedLabel, leg);
    if (document.getElementById('allLabel').classList.contains('active')) {
      document.getElementById('allLabel').classList.remove('active');
      document.getElementById('survivalLabel').classList.add('active');
    }
  }

  selectAll() {
    this.load('all', false);
    document.getElementById('survivalLabel').classList.remove('active');
    document.getElementById('vanillaLabel').classList.remove('active');
    document.getElementById('pvpLabel').classList.remove('active');
    document.getElementById('ecoLabel').classList.remove('active');
    document.getElementById('modLabel').classList.remove('active');
    document.getElementById('creativeLabel').classList.remove('active');
    document.getElementById('legLabel').classList.remove('active');
    document.getElementById('warezLabel').classList.remove('active');
  }

  load(label, leg) {
    this.serverService.getAll(label, leg).then(r => {
      // @ts-ignore
      this.servers.servers = r;
    });
  }

  ngOnInit() {
  }

}

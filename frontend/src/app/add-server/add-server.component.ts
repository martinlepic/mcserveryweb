import {Component, OnInit} from '@angular/core';
import {ServerService} from 'src/services/ServerService';
import {ServerToSave} from '../model/ServerToSave';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-add-server',
  templateUrl: './add-server.component.html',
  styleUrls: ['./add-server.component.css']
})
export class AddServerComponent implements OnInit {

  alerts = [];

  constructor(public serverService: ServerService) {
  }

  ngOnInit() {
  }

  closeAlert() {
    this.alerts = [];
  }

  addServer(addForm: HTMLFormElement, serverName: HTMLInputElement, serverIP: HTMLInputElement, serverPort: HTMLInputElement, serverWeb: HTMLInputElement, serverLabel1: HTMLSelectElement, serverLabel2: HTMLSelectElement, serverOnlineMode: HTMLSelectElement, serverVotifierIP: HTMLInputElement, serverVotifierToken: HTMLInputElement, serverVotifierPort: HTMLInputElement, serverAbout: HTMLTextAreaElement) {
    if (!addForm.checkValidity()) {
      return;
    }
    const server = new ServerToSave(0, serverName.value, serverIP.value, +serverPort.value, serverWeb.value, serverLabel1.value, serverLabel2.value, serverOnlineMode.value, serverVotifierIP.value, +serverVotifierPort.value, serverVotifierToken.value, serverAbout.value, new Date(Date.now()));
    this.serverService.add(server).subscribe(
      (response) => {
        this.alerts = [{
          type: 'success',
          message: 'Server byl úspěšně přidán. Do minuty bude umístěn na server list'
        }];
      },
      (error: HttpErrorResponse) => {
        this.alerts = [{
          type: 'danger',
          message: (error.error as HttpErrorResponse).message
        }];
      }
    );
  }

}

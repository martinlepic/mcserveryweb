import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TopBarComponent} from './top-bar/top-bar.component';
import {ServersContentComponent} from './servers-content/servers-content.component';
import {AdvertisementComponent} from './advertisement/advertisement.component';
import {FooterComponent} from './footer/footer.component';
import {AddServerComponent} from './add-server/add-server.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {AdministrationComponent} from './administration/administration.component';
import {ApiService} from '../services/api.service';
import {HttpClientModule} from '@angular/common/http';
import {ServerService} from '../services/ServerService';
import { FiltersComponent } from './filters/filters.component';
import {FilterService} from '../services/FilterService';
import {FormsModule} from '@angular/forms';
import {NgbAlertModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import { VoteComponent } from './vote/vote.component';
import { EditServerComponent } from './edit-server/edit-server.component';
import {UserService} from '../services/UserService';
import { InfoComponent } from './info/info.component';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    ServersContentComponent,
    AdvertisementComponent,
    FooterComponent,
    AddServerComponent,
    LoginComponent,
    RegisterComponent,
    AdministrationComponent,
    FiltersComponent,
    VoteComponent,
    EditServerComponent,
    InfoComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        NgbPaginationModule,
        NgbAlertModule
    ],
  providers: [ApiService, ServerService, FilterService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule {
}

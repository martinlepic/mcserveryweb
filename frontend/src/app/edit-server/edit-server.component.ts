import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ServerToSave} from '../model/ServerToSave';
import {ServerService} from '../../services/ServerService';
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-edit-server',
  templateUrl: './edit-server.component.html',
  styleUrls: ['./edit-server.component.css']
})
export class EditServerComponent implements OnInit {

  @Input()
  server: ServerToSave;

  alerts = [];

  constructor(public serverService: ServerService) {
  }

  ngOnInit() {
  }

  closeAlert() {
    this.alerts = [];
  }

  editServer(addForm: HTMLFormElement, serverName: HTMLInputElement, serverIP: HTMLInputElement, serverPort: HTMLInputElement, serverWeb: HTMLInputElement, serverLabel1: HTMLSelectElement, serverLabel2: HTMLSelectElement, serverOnlineMode: HTMLSelectElement, serverVotifierIP: HTMLInputElement, serverVotifierToken: HTMLInputElement, serverVotifierPort: HTMLInputElement, serverAbout: HTMLTextAreaElement) {
    if (!addForm.checkValidity()) {
      return;
    }
    const server = new ServerToSave(this.server.id, serverName.value, serverIP.value, +serverPort.value, serverWeb.value, serverLabel1.value, serverLabel2.value, serverOnlineMode.value, serverVotifierIP.value, +serverVotifierPort.value, serverVotifierToken.value, serverAbout.value, new Date(Date.now()));
    this.serverService.editServer(server).subscribe(
      (response) => {
        this.alerts = [{
          type: 'success',
          message: 'Server byl úspěšně editován'
        }];
      },
      (error: HttpErrorResponse) => {
        this.alerts = [{
          type: 'danger',
          message: (error.error as HttpErrorResponse).message
        }];
      }
    );
  }

}

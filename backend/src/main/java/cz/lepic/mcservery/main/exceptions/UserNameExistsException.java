package cz.lepic.mcservery.main.exceptions;

public class UserNameExistsException extends RuntimeException {
    private static final String MESSAGE = "Uživatel s tímto jménem už existuje!";

    public UserNameExistsException() {
        super(MESSAGE);
    }
}

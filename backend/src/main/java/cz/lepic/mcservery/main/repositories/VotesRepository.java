package cz.lepic.mcservery.main.repositories;

import cz.lepic.mcservery.main.entities.Server;
import cz.lepic.mcservery.main.entities.Vote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VotesRepository extends CrudRepository<Vote, Integer> {
    Optional<Integer> countVotesByServer(Server server);
    Optional<Vote> findFirstByServerAndIpOrderByVoteTimeDesc(Server server, String ip);
    void deleteAllByServer(Server server);
}

package cz.lepic.mcservery.main.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Server {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    private User owner;

    private String serverIP;
    private String serverName;
    private String serverWeb;
    private String votToken;
    private String votIp;
    private int serverPort;
    private int votPort;
    private int votes;
    private Date added;
    private Date lastOnline;
    private String version;

    private boolean leg;
    private String label1, label2;
    private String about;

    public Date getLastOnline() {
        return lastOnline;
    }

    public void setLastOnline(Date lastOnline) {
        this.lastOnline = lastOnline;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public Date getAdded() {
        return added;
    }

    public void setAdded(Date added) {
        this.added = added;
    }

    public String getLabel1() {
        return label1;
    }

    public void setLabel1(String label1) {
        this.label1 = label1;
    }

    public String getLabel2() {
        return label2;
    }

    public void setLabel2(String label2) {
        this.label2 = label2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerWeb() {
        return serverWeb;
    }

    public void setServerWeb(String serverWeb) {
        this.serverWeb = serverWeb;
    }

    public String getVotToken() {
        return votToken;
    }

    public void setVotToken(String votToken) {
        this.votToken = votToken;
    }

    public String getVotIp() {
        return votIp;
    }

    public void setVotIp(String votIp) {
        this.votIp = votIp;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public int getVotPort() {
        return votPort;
    }

    public void setVotPort(int votPort) {
        this.votPort = votPort;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public boolean isLeg() {
        return leg;
    }

    public void setLeg(boolean leg) {
        this.leg = leg;
    }


    public Server() {
    }

    public Server(int id, String serverIP, String serverName, String serverWeb, String votToken, String votIp,
                  int serverPort, int votPort, String version, boolean leg, String label1, String label2,
                  String about, int votes, Date added, Date lastOnline, User owner) {
        this.id = id;
        this.serverIP = serverIP;
        this.serverName = serverName;
        this.serverWeb = serverWeb;
        this.votToken = votToken;
        this.votIp = votIp;
        this.serverPort = serverPort;
        this.votPort = votPort;
        this.version = version;
        this.leg = leg;
        this.label1 = label1;
        this.label2 = label2;
        this.about = about;
        this.votes = votes;
        this.added = added;
        this.owner = owner;
        this.lastOnline = lastOnline;
    }
}

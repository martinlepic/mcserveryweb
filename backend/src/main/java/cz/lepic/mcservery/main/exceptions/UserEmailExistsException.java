package cz.lepic.mcservery.main.exceptions;

public class UserEmailExistsException extends RuntimeException {
    public static final String MESSAGE = "Uživatel s tímto e-mailem už existuje!";

    public UserEmailExistsException() {
        super(MESSAGE);
    }
}

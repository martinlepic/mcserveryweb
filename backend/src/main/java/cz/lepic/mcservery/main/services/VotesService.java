package cz.lepic.mcservery.main.services;

import cz.lepic.mcservery.main.entities.Server;
import cz.lepic.mcservery.main.entities.Vote;
import cz.lepic.mcservery.main.exceptions.CantVoteException;
import cz.lepic.mcservery.main.exceptions.ServerIdException;
import cz.lepic.mcservery.main.repositories.ServerRepository;
import cz.lepic.mcservery.main.repositories.VotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Service
public class VotesService {
    @Autowired
    private VotesRepository votesRepository;
    @Autowired
    private ServerRepository serverRepository;

    public void vote(int id, String nickname, String ip) {
        Server server = serverRepository.findById(id).orElseThrow(ServerIdException::new);
        if (!canVote(ip, server)) {
            throw new CantVoteException();
        }
        Vote vote = new Vote(0, ip, nickname, server, new Date());
        votesRepository.save(vote);
        checkVotes(server);
    }

    private void checkVotes(Server server) {
        int votes = votesRepository.countVotesByServer(server).get();
        server.setVotes(votes);
        serverRepository.save(server);
    }

    private boolean canVote(String ip, Server server) {
        Vote vote;
        try {
            vote = votesRepository.findFirstByServerAndIpOrderByVoteTimeDesc(server, ip).get();
        } catch (RuntimeException e) {
            return true;
        }
        long diffTime = new Date().getTime() - vote.getVoteTime().getTime();
        long minutes = TimeUnit.MILLISECONDS.toMinutes(diffTime);
        if (minutes > 60) {
            return true;
        }
        return false;
    }
}

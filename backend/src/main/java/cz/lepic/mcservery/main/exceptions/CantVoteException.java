package cz.lepic.mcservery.main.exceptions;

public class CantVoteException extends RuntimeException {
    private static final String MESSAGE = "Už jsi hlasoval, vrať se za hodinu!";

    public CantVoteException() {
        super(MESSAGE);
    }
}

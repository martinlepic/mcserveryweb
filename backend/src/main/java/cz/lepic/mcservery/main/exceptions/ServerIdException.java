package cz.lepic.mcservery.main.exceptions;

public class ServerIdException extends RuntimeException {
    private static final String MESSAGE = "Server s tímto ID neexistuje!";

    public ServerIdException() {
        super(MESSAGE);
    }
}

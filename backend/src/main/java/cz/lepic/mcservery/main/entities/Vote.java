package cz.lepic.mcservery.main.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Vote {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String ip, player;
    @ManyToOne(fetch = FetchType.LAZY)
    private Server server;
    private Date voteTime;

    public Vote() {

    }

    public Vote(int id, String ip, String player, Server server, Date voteTime) {
        this.id = id;
        this.ip = ip;
        this.player = player;
        this.server = server;
        this.voteTime = voteTime;
    }

    public Date getVoteTime() {
        return voteTime;
    }

    public void setVoteTime(Date voteTime) {
        this.voteTime = voteTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }
}

package cz.lepic.mcservery.main.exceptions;

public class ServerAlreadyExistsException extends RuntimeException {
    public static final String MESSAGE = "Server s touto IP už existuje!";

    public ServerAlreadyExistsException() {
        super(MESSAGE);
    }
}

package cz.lepic.mcservery.main.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServerToShow {
    private int id;
    private boolean online, onlineMode;
    private String serverName, serverIP, serverWeb, version, label1, label2, about;
    private int serverPort, maxPlayers, onlinePlayers, position, votes;
    private Date added;
}

package cz.lepic.mcservery.main.controllers;

import cz.lepic.mcservery.main.entities.User;
import cz.lepic.mcservery.main.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value="/register", method = RequestMethod.POST)
    public boolean register(@RequestBody User user) {
        return userService.Register(user);
    }
}

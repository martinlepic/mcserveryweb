package cz.lepic.mcservery.main.repositories;

import cz.lepic.mcservery.main.entities.Server;
import cz.lepic.mcservery.main.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ServerRepository extends CrudRepository<Server, Integer> {
    Optional<Server> findById(int id);
    Optional<Iterable<Server>> findAllByOwner(User owner);
    void deleteById(int id);
    int countAllByServerIP(String serverIp);
}

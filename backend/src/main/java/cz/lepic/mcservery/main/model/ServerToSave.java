package cz.lepic.mcservery.main.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServerToSave {
    private int id;
    private String serverName, serverIP, serverWeb, label1, label2, onlineMode, votifierIP, votifierToken, about;
    private int serverPort, votifierPort;
    private Date added;

}

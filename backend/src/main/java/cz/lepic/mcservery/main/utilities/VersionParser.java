package cz.lepic.mcservery.main.utilities;

public class VersionParser {
    public static String getNiceVersion(String version) {
        String[] verArray = version.split(" ");
        return verArray[verArray.length-1];
    }
}

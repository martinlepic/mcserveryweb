package cz.lepic.mcservery.main.controllers;

import cz.lepic.mcservery.main.services.FilterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping(path = "filters")
public class FilterController {

    @Autowired
    private FilterService filterService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public HashMap<String, Integer> getAll() {
        return filterService.getCounts();
    }

}

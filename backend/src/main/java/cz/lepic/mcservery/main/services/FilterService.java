package cz.lepic.mcservery.main.services;

import cz.lepic.mcservery.main.entities.Server;
import cz.lepic.mcservery.main.model.ServerToShow;
import cz.lepic.mcservery.main.repositories.ServerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

@Service
public class FilterService {

    @Autowired
    private ServerRepository serverRepository;

    public HashMap<String, Integer> getCounts() {
        HashMap<String, Integer> counts = new HashMap<String, Integer>();
        Iterable<Server> servers = serverRepository.findAll();

        counts.put("Survival", 0);
        counts.put("Vanilla", 0);
        counts.put("PvP", 0);
        counts.put("Economy", 0);
        counts.put("Minihry", 0);
        counts.put("Creative", 0);
        counts.put("Leg", 0);
        counts.put("Warez", 0);

        int all = 0;
        for (Server server : servers) {
            all++;
            switch (server.getLabel1()) {
                case "Survival":
                    counts.put("Survival", counts.get("Survival") + 1);
                    break;
                case "Vanilla":
                    counts.put("Vanilla", counts.get("Vanilla") + 1);
                    break;
                case "PvP":
                    counts.put("PvP", counts.get("PvP") + 1);
                    break;
                case "Economy":
                    counts.put("Economy", counts.get("Economy") + 1);
                    break;
                case "Creative":
                    counts.put("Creative", counts.get("Creative") + 1);
                    break;
                case "Minihry":
                    counts.put("Minihry", counts.get("Minihry") + 1);
                    break;
            }
            switch (server.getLabel2()) {
                case "Survival":
                    counts.put("Survival", counts.get("Survival") + 1);
                    break;
                case "Vanilla":
                    counts.put("Vanilla", counts.get("Vanilla") + 1);
                    break;
                case "PvP":
                    counts.put("PvP", counts.get("PvP") + 1);
                    break;
                case "Economy":
                    counts.put("Economy", counts.get("Economy") + 1);
                    break;
                case "Creative":
                    counts.put("Creative", counts.get("Creative") + 1);
                    break;
                case "Minihry":
                    counts.put("Minihry", counts.get("Minihry") + 1);
                    break;
            }
            if (server.isLeg()) {
                counts.put("Leg", counts.get("Leg") + 1);
            } else {
                counts.put("Warez", counts.get("Warez") + 1);
            }
        }
        counts.put("All", all);
        return counts;
    }

    public ArrayList<ServerToShow> filter(ArrayList<ServerToShow> servers, String label, boolean leg) {
        ArrayList<ServerToShow> results = servers.stream().filter(server ->
                (server.getLabel1().equals(label) || server.getLabel2().equals(label)) &&
                server.isOnlineMode() == leg).collect(Collectors.toCollection(ArrayList::new));

        return results;
    }
}

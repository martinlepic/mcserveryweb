package cz.lepic.mcservery.main.services;

import cz.lepic.mcservery.main.entities.Server;
import cz.lepic.mcservery.main.entities.User;
import cz.lepic.mcservery.main.exceptions.ServerAlreadyExistsException;
import cz.lepic.mcservery.main.mappers.ServerMapper;
import cz.lepic.mcservery.main.model.ServerToSave;
import cz.lepic.mcservery.main.model.ServerToShow;
import cz.lepic.mcservery.main.model.ServerInfo;
import cz.lepic.mcservery.main.repositories.ServerRepository;
import cz.lepic.mcservery.main.repositories.UserRepository;
import cz.lepic.mcservery.main.repositories.VotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

@Service
@EnableAsync
@EnableScheduling
public class ServerService {
    private final String XDEFCON_API_URL = "https://eu.mc-api.net/v3/server/ping/";

    @Autowired
    private FilterService filterService;
    @Autowired
    private ServerRepository serverRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VotesRepository votesRepository;
    private ServerMapper serverMapper;
    private ArrayList<ServerToShow> serversToShow;

    public ServerService() {
        serverMapper = new ServerMapper();
    }

    public ArrayList<ServerToShow> getAll(String label, boolean leg) {
        if (label.equals("all")) {
            return serversToShow;
        }
        return filterService.filter(serversToShow, label, leg);
    }

    @Transactional
    public void deleteServer(Principal me, int serverId) {
        Server server = serverRepository.findById(serverId).get();
        User owner = userRepository.findByUsername(me.getName()).get();
        if (server.getOwner() == owner) {
            votesRepository.deleteAllByServer(server);
            serverRepository.deleteById(serverId);
        }
    }

    public void editServer(Principal me, ServerToSave server) {
        User owner = userRepository.findByUsername(me.getName()).get();
        Server serverOld = serverRepository.findById(server.getId()).get();
        if (serverOld.getOwner() == owner) {
            int servers = serverRepository.countAllByServerIP(server.getServerIP());
            if (servers > 0 && !server.getServerIP().equals(serverOld.getServerIP())) {
                throw new ServerAlreadyExistsException();
            }
            serverOld.setServerName(server.getServerName());
            serverOld.setServerIP(server.getServerIP());
            serverOld.setServerPort(server.getServerPort());
            serverOld.setServerWeb(server.getServerWeb());
            serverOld.setLabel1(server.getLabel1());
            serverOld.setLabel2(server.getLabel2());
            boolean onlineMode = false;
            if (server.getOnlineMode().equals("Leg")) {
                onlineMode = true;
            }
            serverOld.setLeg(onlineMode);
            serverOld.setVotIp(server.getVotifierIP());
            serverOld.setVotToken(server.getVotifierToken());
            serverOld.setVotPort(server.getVotifierPort());
            serverOld.setAbout(server.getAbout());

            serverRepository.save(serverOld);
        }
    }

    public ArrayList<ServerToSave> getUsersServers(Principal me) {
        User owner = userRepository.findByUsername(me.getName()).get();
        Iterable<Server> servers = serverRepository.findAllByOwner(owner).get();
        ArrayList<ServerToSave> results = new ArrayList<>();
        for (Server server:servers) {
            results.add(serverMapper.serverToServerToSave(server));
        }

        return results;
    }

    public String getServerName(int id) {
        return serverRepository.findById(id).get().getServerName();
    }

    public void add(ServerToSave serverToSave, Principal me) {
        int servers = serverRepository.countAllByServerIP(serverToSave.getServerIP());
        if (servers > 0) {
            throw new ServerAlreadyExistsException();
        }
        User owner = userRepository.findByUsername(me.getName()).get();
        serverRepository.save(serverMapper.serverToSaveToServerEntity(serverToSave, owner));
    }

    private ServerInfo getServerInfo(String ip, int port) {
        if (port != 0) {
            ip = ip + ":" + port;
        }

        final String uri = XDEFCON_API_URL + ip;
        RestTemplate restTemplate = new RestTemplate();
        ServerInfo result = restTemplate.getForObject(uri, ServerInfo.class);

        return result;
    }

    @Async
    @Scheduled(fixedDelay = 1000*60, initialDelay = 1000)
    public void queryServers() {
        Iterable<Server> servers = serverRepository.findAll();
        ArrayList<ServerToShow> serversToShow = new ArrayList<>();
        for (Server server : servers) {
            ServerInfo si = getServerInfo(server.getServerIP(), server.getServerPort());
            if (si.getOnline()) {
                server.setLastOnline(new Date());
                serverRepository.save(server);
            }
            serversToShow.add(serverMapper.serverInfoToServerToShow(server, si));
        }
        this.serversToShow = serversToShow;
        countRank();
    }

    private void countRank() {
        Collections.sort(serversToShow, new Comparator<ServerToShow>() {
            @Override
            public int compare(ServerToShow o1, ServerToShow o2) {
                return o1.getVotes() - (o2.getVotes());
            }
        }.reversed());
        int pos = 1;
        for (ServerToShow s:serversToShow) {
            s.setPosition(pos);
            pos++;
        }
    }
}

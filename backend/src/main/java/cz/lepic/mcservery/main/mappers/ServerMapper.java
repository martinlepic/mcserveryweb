package cz.lepic.mcservery.main.mappers;

import cz.lepic.mcservery.main.entities.Server;
import cz.lepic.mcservery.main.entities.User;
import cz.lepic.mcservery.main.model.ServerInfo;
import cz.lepic.mcservery.main.model.ServerToSave;
import cz.lepic.mcservery.main.model.ServerToShow;
import cz.lepic.mcservery.main.utilities.VersionParser;

public class ServerMapper {
    public Server serverToSaveToServerEntity(ServerToSave serverToSave, User owner) {
        boolean onlineMode = false;
        if (serverToSave.getOnlineMode().equals("Leg")) {
            onlineMode = true;
        }
        return new Server(0, serverToSave.getServerIP(), serverToSave.getServerName(), serverToSave.getServerWeb(),
                serverToSave.getVotifierToken(), serverToSave.getVotifierIP(), serverToSave.getServerPort(),
                serverToSave.getVotifierPort(), null, onlineMode, serverToSave.getLabel1(), serverToSave.getLabel2(), serverToSave.getAbout(), 0, serverToSave.getAdded(), serverToSave.getAdded(), owner);
    }

    public ServerToSave serverToServerToSave(Server server) {
        String onlineMode = "Leg";
        if (!server.isLeg()) {
            onlineMode = "Warez";
        }
        return ServerToSave.builder().about(server.getAbout())
                .added(server.getAdded())
                .label1(server.getLabel1())
                .label2(server.getLabel2())
                .onlineMode(onlineMode)
                .serverIP(server.getServerIP())
                .serverName(server.getServerName())
                .serverPort(server.getServerPort())
                .serverWeb(server.getServerWeb())
                .votifierIP(server.getVotIp())
                .votifierPort(server.getVotPort())
                .votifierToken(server.getVotToken())
                .id(server.getId())
                .build();
    }

    public ServerToShow serverInfoToServerToShow(Server server, ServerInfo serverInfo) {
        ServerToShow serverToShow;
        if (serverInfo.getOnline()) {
            serverToShow = ServerToShow.builder().id(server.getId())
                    .online(serverInfo.getOnline())
                    .serverName(server.getServerName())
                    .maxPlayers(serverInfo.getPlayers().getMax())
                    .onlinePlayers(serverInfo.getPlayers().getOnline())
                    .serverIP(server.getServerIP())
                    .serverPort(server.getServerPort())
                    .serverWeb(server.getServerWeb())
                    .label1(server.getLabel1())
                    .label2(server.getLabel2())
                    .onlineMode(server.isLeg())
                    .about(server.getAbout())
                    .added(server.getAdded())
                    .votes(server.getVotes())
                    .version(VersionParser.getNiceVersion(serverInfo.getVersion().getName())).build();
        } else {
            serverToShow = ServerToShow.builder().id(server.getId())
                    .online(serverInfo.getOnline())
                    .serverName(server.getServerName())
                    .serverIP(server.getServerIP())
                    .serverPort(server.getServerPort())
                    .label1(server.getLabel1())
                    .label2(server.getLabel2())
                    .onlineMode(server.isLeg())
                    .about(server.getAbout())
                    .added(server.getAdded())
                    .votes(server.getVotes())
                    .serverWeb(server.getServerWeb()).build();
        }
        return serverToShow;
    }
}

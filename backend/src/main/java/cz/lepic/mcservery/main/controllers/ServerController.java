package cz.lepic.mcservery.main.controllers;

import cz.lepic.mcservery.main.entities.Server;
import cz.lepic.mcservery.main.model.ServerToSave;
import cz.lepic.mcservery.main.model.ServerToShow;
import cz.lepic.mcservery.main.services.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;

@RestController
@RequestMapping(path = "servers")
public class ServerController {

    @Autowired
    private ServerService serverService;

    @RequestMapping(value = "/all/{label}/{leg}", method = RequestMethod.GET)
    public ArrayList<ServerToShow> getAll(@PathVariable String label, @PathVariable boolean leg) {
        return serverService.getAll(label, leg);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void add(@AuthenticationPrincipal Principal me, @RequestBody ServerToSave server) {
        serverService.add(server, me);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public void add(@AuthenticationPrincipal Principal me, @PathVariable int id) {
        serverService.deleteServer(me, id);
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public void edit(@AuthenticationPrincipal Principal me, @RequestBody ServerToSave server) {
        serverService.editServer(me, server);
    }

    @RequestMapping(value = "/users-servers", method = RequestMethod.GET)
    public ArrayList<ServerToSave> getUsersServers(@AuthenticationPrincipal Principal me) {
        return serverService.getUsersServers(me);
    }

    @RequestMapping(value = "/server-name/{id}", method = RequestMethod.GET)
    public String getServerName(@PathVariable int id) {
        return serverService.getServerName(id);
    }
}
